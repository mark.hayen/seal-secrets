package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
)

// sealCmd represents the seal command
var sealCmd = &cobra.Command{
	Use:   "seal",
	Short: "Scans provided value files against secret templates to see if any of the values require encryption",
	Long:  `Seal is the primary command for seal-secrets. etc.`,
	Args: func(cmd *cobra.Command, args []string) error {
		if err := cobra.MinimumNArgs(1)(cmd, args); err != nil {
			return err
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		chartPath := args[0]
		certPath, err := cmd.Flags().GetString("certpath")
		if err != nil {
			log.Fatal(err)
		}
		valuesFileNames, err := cmd.Flags().GetStringSlice("valuefiles")
		if err != nil {
			log.Fatal(err)
		}
		secretNamespace, err := cmd.Flags().GetString("namespace")
		if err != nil {
			log.Fatal(err)
		}
		secretName, err := cmd.Flags().GetString("name")
		if err != nil {
			log.Fatal(err)
		}

		useBase64, err := cmd.Flags().GetBool("base64")
		if err != nil {
			log.Fatal(err)
		}
		c := newChart(chartPath, certPath, valuesFileNames, secretNamespace, secretName)
		seal(c, useBase64)
	},
}

func init() {
	rootCmd.AddCommand(sealCmd)
	sealCmd.Flags().StringSliceP("valuefiles", "v", nil, "please provide value file names relative to test-chart path as a comma-separated list")
	sealCmd.Flags().StringP("certpath", "c", "http://sealed-secrets.local.rhos-ota.tnl-edsn.nl/v1/cert.pem", "specify a valid cert path")
	sealCmd.Flags().StringP("namespace", "s", "", "namespace to use for this specific seal operation. Overrides value file secretNamespace values")
	sealCmd.Flags().StringP("name", "n", "", "name to use for this specific seal operation. Overrides value file secretName values")
	sealCmd.Flags().BoolP("base64", "b", false, "Determines whether data is to be encrypted as plain text or base64. Default is plain text")
}

func seal(c *chart, useBase64 bool) {
	// get secretValueKeys and save them
	secretTemplateVars := make(map[string]string)
	templatePaths, err := c.getSecretTemplatePaths()
	if err != nil {
		log.Fatalf("Failed to read secret templates: %s", err.Error())
	}

	for _, path := range templatePaths {
		f, err := os.ReadFile(path)
		if err != nil {
			log.Fatal(err)
		}

		m, err := c.getTemplateVars(string(f))
		if err != nil {
			log.Fatal(err)
		}

		secretTemplateVars = mergeStringMaps(secretTemplateVars, m)
	}

	// get value files, see if anyting needs encrypting, and encrypt it
	for _, name := range c.valueFiles {
		log.Printf("scanning %s...", name)
		vf, err := openValueFile(c.path, name)
		if err != nil {
			log.Fatal(err)
		}

		sf, err := openSealedFile(c.path)
		if err != nil {
			log.Fatal(err)
		}

		secretNamespace := c.secretNamespace
		secretName := c.secretName

		if secretNamespace == "" {
			value, ok := vf.findValueByKeyString("secretNamespace")
			if !ok {
				log.Fatal(fmt.Errorf("missing or malformed value for required key 'secretNamespace' in %s", name))
			}

			secretNamespace = value.(string)
		}

		if secretName == "" {
			value, ok := vf.findValueByKeyString("secretName")
			if !ok {
				log.Fatal(fmt.Errorf("missing or malformed value for required key 'secretName' in %s", name))
			}

			secretName = value.(string)
		}

		plainTmpl := newPlainTemplate(secretName, secretNamespace)
		deltas := make(map[string]string)

		// find secret by secret value key
		for valueKeyStr, tmplKey := range secretTemplateVars {
			if sf.containsKeyStrForValueFile(valueKeyStr, vf) {
				continue
			}

			v, ok := vf.findValueByKeyString(valueKeyStr)
			if !ok {
				continue
			}

			var secretValue string
			if secretValue, ok = v.(string); !ok {
				log.Fatalf("expected string value for %s, got object %v instead", valueKeyStr, v)
			}

			plainTmpl.stageSecret(tmplKey, secretValue, useBase64)

			// add staged keys to writeOuts so that they can be properly persisted to value files later
			deltas[tmplKey] = valueKeyStr
		}

		if len(plainTmpl.Data) == 0 {
			log.Printf("found no new secrets in values file %s", name)
			continue
		}

		encrypted, err := plainTmpl.encrypt(c.certPath)
		if err != nil {
			log.Fatalf("failed to encrypt: %s", err)
		}

		// write encrypted values out to value files
		for tmplKey, keyStr := range deltas {
			err := vf.setValueByKeyString(keyStr, encrypted.Spec.EncryptedData[tmplKey])
			if err != nil {
				log.Fatal(err)
			}
			sf.addSecretValueKey(name, keyStr)
		}

		err = vf.save()
		if err != nil {
			log.Fatalf("Failed to write out encrypted values to file %s: %s", vf.name, err.Error())
		}

		err = sf.save()
		if err != nil {
			log.Fatalf("Failed to write to _sealed.yaml: %s", err.Error())
		}

		log.Printf("succesfully encrypted values for values file %s", name)
	}
}

package cmd

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetTemplateVars(t *testing.T) {
	type test struct {
		desc     string
		template string
		exp      map[string]string
	}

	tests := []test{
		{
			"trivial",
			`
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  name: test-chart
  namespace: test-namespace
spec:
  encryptedData:
    mySecret: {{ .Values.mySecret }}
  template:
    metadata:
      name: test-chart
      namespace:  test-namespace
`,
			map[string]string{"mySecret": "mySecret"},
		},
		{
			"multiple keys",
			`
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  name: test-chart
  namespace: test-namespace
spec:
  encryptedData:
    mySecret: {{ .Values.mySecret }}
    mySecret2: {{ .Values.mySecret2 }}
  template:
    metadata:
      name: test-chart
      namespace:  test-namespace
`,
			map[string]string{
				"mySecret":  "mySecret",
				"mySecret2": "mySecret2",
			},
		},
		{
			"containing conditionals",
			`

{{- if .Values.myBool }}
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  name: test-chart
  namespace: test-namespace
spec:
  encryptedData:
    mySecret: {{ .Values.mySecret }}
  template:
    metadata:
      name: test-chart
      namespace:  test-namespace
{{- end }}
`,
			map[string]string{
				"mySecret": "mySecret",
			},
		},
		{
			"nested value file references",
			`

apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  name: test-chart
  namespace: test-namespace
spec:
  encryptedData:
    mySecret: {{ .Values.someObject.mySecret }}
  template:
    metadata:
      name: test-chart
      namespace:  test-namespace
`,
			map[string]string{
				"someObject.mySecret": "mySecret",
			},
		},
		{
			"shouldn't care about helm functions",
			`

apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  name: {{ include "something" . }}
  namespace: test-namespace
spec:
  encryptedData:
    mySecret: {{ .Values.someObject.mySecret }}
  template:
    metadata:
      name: test-chart
      namespace:  test-namespace
`,
			map[string]string{
				"someObject.mySecret": "mySecret",
			},
		},
	}

	for _, tst := range tests {
		t.Run(tst.desc, func(t *testing.T) {
			c := newChart("", "", nil, "", "")
			keys, err := c.getTemplateVars(tst.template)
			if err != nil {
				t.Error(err)
			}
			assert.Equal(t, tst.exp, keys)
		})
	}
}

func TestChart_isSecretTemplate(t *testing.T) {
	templateString := `
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  name: test-chart
  namespace: test-namespace
spec:
  encryptedData:
    mySecret: {{ .Values.mySecret }}
  template:
    metadata:
      name: test-chart
      namespace:  test-namespace
`

	assert.Equal(t, isSecretTemplate(templateString), true)
}

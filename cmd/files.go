package cmd

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
	"strings"
)

type valueFile struct {
	*os.File
	name     string
	document *yaml.Node
}

func openValueFile(chartPath, name string) (*valueFile, error) {
	f, err := os.OpenFile(filepath.Join(chartPath, name), os.O_RDWR, 0755)
	if err != nil {
		return nil, err
	}

	b, err := os.ReadFile(f.Name())
	if err != nil {
		return nil, err
	}

	vf := &valueFile{
		f,
		name,
		&yaml.Node{},
	}

	err = yaml.Unmarshal(b, vf.document)
	if err != nil {
		return nil, err
	}

	return vf, nil
}

func (vf *valueFile) save() error {
	defer vf.Close()

	out, err := yaml.Marshal(vf.document)
	if err != nil {
		return err
	}

	_, err = vf.Write(out)
	if err != nil {
		return err
	}

	return nil
}

func (vf *valueFile) findValueByKeyString(keyStr string) (any, bool) {
	keys := strings.Split(keyStr, ".")
	rootNode := vf.document.Content[0] // root node

	if node, ok := findValueNodeForKeyList(keys, rootNode); ok {
		return node.Value, ok
	}

	return nil, false
}

func findValueNodeForKeyList(keys []string, node *yaml.Node) (*yaml.Node, bool) {
	for n, currentNode := range node.Content {
		if currentNode.Kind == yaml.ScalarNode {
			if currentNode.Value == keys[0] {
				if node.Content[n+1].Kind == yaml.ScalarNode && len(keys) == 1 {
					return node.Content[n+1], true
				}

				if node.Content[n+1].Kind == yaml.MappingNode && len(keys) > 1 {
					return findValueNodeForKeyList(keys[1:], node.Content[n+1])
				}
			}
		}
	}
	return nil, false
}

func (vf *valueFile) setValueByKeyString(keyStr string, secretValue string) error {
	keys := strings.Split(keyStr, ".")
	rootNode := vf.document.Content[0] // root node

	node, ok := findValueNodeForKeyList(keys, rootNode)
	if !ok {
		return fmt.Errorf("no key for key string %s in document %v", keyStr, rootNode)
	}

	node.Value = secretValue
	return nil
}

type sealedFile struct {
	*os.File
	path     string
	contents map[string][]string
}

func openSealedFile(chartPath string) (*sealedFile, error) {
	path := filepath.Join(chartPath, "_sealed.yaml")

	var (
		f   *os.File
		err error
	)

	f, err = os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		//if err == os.ErrNotExist {
		//	f, err = os.Create(path)
		//	if err != nil {
		//		return nil, err
		//	}
		//}
		return nil, err
	}

	b, err := os.ReadFile(f.Name())
	if err != nil {
		return nil, err
	}

	sf := sealedFile{
		File:     f,
		path:     path,
		contents: make(map[string][]string),
	}

	err = yaml.Unmarshal(b, sf.contents)
	if err != nil {
		return nil, err
	}

	return &sf, nil
}

func (sf *sealedFile) addSecretValueKey(valueFileName, item string) {
	name := strings.Split(valueFileName, ",")[0]
	sf.contents[name] = append(sf.contents[name], item)
}

func (sf *sealedFile) save() error {
	b, err := yaml.Marshal(sf.contents)
	if err != nil {
		return err
	}

	return os.WriteFile(sf.path, b, 0666)
}

func (sf *sealedFile) containsKeyStrForValueFile(keyStr string, vf *valueFile) bool {
	for _, v := range sf.contents[vf.name] {
		if v == keyStr {
			return true
		}
	}

	return false
}

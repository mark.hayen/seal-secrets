package cmd

import (
	"bytes"
	"errors"
	"gopkg.in/yaml.v3"
	"helm.sh/helm/v3/pkg/chart/loader"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"
)

type chart struct {
	path       string
	certPath   string
	valueFiles []string

	// value file overrides
	secretName      string
	secretNamespace string
}

func newChart(path, certPath string, valueFiles []string, secretNamespace, secretName string) *chart {
	return &chart{
		path,
		certPath,
		valueFiles,
		secretName,
		secretNamespace,
	}
}

// getTemplateVars extracts variable names from the secret templates
func (c *chart) getTemplateVars(template string) (map[string]string, error) {
	// regex to match Go template variables
	variableRegex := regexp.MustCompile(`{{\s*\..*?(\w+)\s*}}`)
	matches := variableRegex.FindAllStringSubmatch(template, -1)

	if len(matches) == 0 {
		return nil, errors.New("no variables found in template")
	}

	vars := make(map[string]string)

	for _, match := range matches {
		varName := match[1]

		templateVarName := strings.Trim(match[0], "{} ")
		templateVarName = strings.TrimPrefix(templateVarName, ".Values.")

		// check if the variable name already exists in the map
		if _, ok := vars[varName]; ok {
			return nil, errors.New("duplicate variable name found in template")
		}

		vars[templateVarName] = varName
	}

	return vars, nil
}

func (c *chart) getSecretTemplatePaths() ([]string, error) {
	chart, err := loader.Load(c.path)
	if err != nil {
		return nil, err
	}

	var paths []string
	for _, tmpl := range chart.Templates {
		tmplPath := filepath.Join(c.path, tmpl.Name)
		f, err := os.ReadFile(tmplPath)
		if err != nil {
			return nil, err
		}

		// check that the template Kind is 'SealedSecret'
		if !isSecretTemplate(string(f)) {
			continue
		}

		paths = append(paths, tmplPath)
	}

	return paths, nil
}

func isSecretTemplate(template string) bool {
	kindRegex := regexp.MustCompile(`(?m)^kind:\s*SealedSecret\s*$`)
	return kindRegex.MatchString(template)
}

func (c *chart) parseRawTemplate(raw string) (*EncryptedTemplate, error) {
	// instantiate template based off of raw string
	tmpl, err := template.New("").Parse(raw)
	if err != nil {
		return nil, err
	}

	// create container for values
	data := make(map[string]map[string]any)
	data["Values"] = make(map[string]any)

	for _, filename := range c.valueFiles {
		f, err := os.ReadFile(filepath.Join(c.path, filename))
		if err != nil {
			return nil, err
		}

		m := make(map[string]any)
		err = yaml.Unmarshal(f, m)
		if err != nil {
			return nil, err
		}

		// add unmarshalled yaml to values, overwriting any duplicate values
		// we do this mostly so that all fields exist among the values and things like conditionals aren't accidentally omitted
		data["Values"] = mergeMaps(data["Values"], m)
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, data)
	if err != nil {
		return nil, err
	}

	var secretTemplate EncryptedTemplate
	err = yaml.Unmarshal(buf.Bytes(), &secretTemplate)
	if err != nil {
		return nil, err
	}

	return &secretTemplate, nil
}

package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"os"
)

// sealCmd represents the seal command
var initializeCmd = &cobra.Command{
	Use:   "init",
	Short: "Scans provided value files against secret templates to and sets any discovered values as already encrypted in _sealed.yaml",
	Long:  `init is meant to initialize an existing chart's _sealed.yaml file, so that any subsequent seals will recognize already encrypted values'`,
	Args: func(cmd *cobra.Command, args []string) error {
		if err := cobra.MinimumNArgs(1)(cmd, args); err != nil {
			return err
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		chartPath := args[0]
		valuesFileNames, err := cmd.Flags().GetStringSlice("valuefiles")
		if err != nil {
			log.Fatal(err)
		}

		c := newChart(chartPath, "", valuesFileNames, "", "")
		initialize(c)
	},
}

func init() {
	rootCmd.AddCommand(initializeCmd)
	initializeCmd.Flags().StringSliceP("valuefiles", "v", nil, "please provide value file names relative to test-chart path as a comma-separated list")
}

// initialize is run against a chart that already contains sealed secrets
// instead of figuring out which secrets need to be sealed, it will assume that any secret it encounters is a sealed secret
// it instantiates a _sealed.yaml so that future secrets are recognized
func initialize(c *chart) {
	secretTemplateVars := make(map[string]string)
	templatePaths, err := c.getSecretTemplatePaths()
	if err != nil {
		log.Fatalf("Failed to read secret templates: %s", err.Error())
	}

	for _, path := range templatePaths {
		f, err := os.ReadFile(path)
		if err != nil {
			log.Fatal(err)
		}

		m, err := c.getTemplateVars(string(f))
		if err != nil {
			log.Fatal(err)
		}

		secretTemplateVars = mergeStringMaps(secretTemplateVars, m)
	}

	sf, err := openSealedFile(c.path)
	if err != nil {
		log.Fatal(err)
	}

	for _, name := range c.valueFiles {
		log.Printf("scanning %s...", name)
		vf, err := openValueFile(c.path, name)
		if err != nil {
			log.Fatal(err)
		}

		for valueKeyStr := range secretTemplateVars {
			if sf.containsKeyStrForValueFile(valueKeyStr, vf) {
				continue
			}

			if _, ok := vf.findValueByKeyString(valueKeyStr); !ok {
				log.Printf("no value found for '%s' in %s. skipping..", valueKeyStr, name)
				continue
			}

			log.Printf("encrypted value found for '%s' in %s. appending to _sealed.yaml..", valueKeyStr, name)
			sf.addSecretValueKey(name, valueKeyStr)
		}
	}

	if err := sf.save(); err != nil {
		log.Fatalf("failed to initialize chart: %s", err)
	}

	log.Println("succesfully initialized chart")
}

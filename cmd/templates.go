package cmd

import (
	"encoding/base64"
	"gopkg.in/yaml.v3"
	"os"
	"os/exec"
)

// EncryptedTemplate represents the secret.yaml template file
type EncryptedTemplate struct {
	APIVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	} `yaml:"metadata"`
	Spec struct {
		EncryptedData map[string]string `yaml:"encryptedData"`
	} `yaml:"spec"`
}

func newEncryptedTemplate() *EncryptedTemplate {
	return &EncryptedTemplate{
		APIVersion: "",
		Kind:       "",
		Metadata: struct {
			Name      string `yaml:"name"`
			Namespace string `yaml:"namespace"`
		}{},
		Spec: struct {
			EncryptedData map[string]string `yaml:"encryptedData"`
		}{
			EncryptedData: make(map[string]string),
		},
	}
}

// PlainTemplate represents the unencrypted secret.yaml template
type PlainTemplate struct {
	APIVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	} `yaml:"metadata"`
	Data map[string]string `yaml:"data"`
	Type string            `yaml:"type"`
}

func newPlainTemplate(metadataName, metadataNamespace string) *PlainTemplate {
	return &PlainTemplate{
		APIVersion: "v1",
		Kind:       "Secret",
		Metadata: struct {
			Name      string `yaml:"name"`
			Namespace string `yaml:"namespace"`
		}{
			Name:      metadataName,
			Namespace: metadataNamespace,
		},
		Data: make(map[string]string),
		Type: "Opaque",
	}
}

func (tmpl *PlainTemplate) stageSecret(key, value string, useBase64 bool) {
	if !useBase64 {
		tmpl.Data[key] = base64.StdEncoding.EncodeToString([]byte(value))
	} else {
		tmpl.Data[key] = value
	}
}

func (tmpl *PlainTemplate) encrypt(certPath string) (*EncryptedTemplate, error) {
	b, err := yaml.Marshal(*tmpl)
	if err != nil {
		return nil, err
	}

	f, err := os.CreateTemp("", "plain")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	_, err = f.Write(b)
	if err != nil {
		return nil, err
	}

	out, err := exec.Command("kubeseal", "--cert", certPath, "-f", f.Name(), "-o", "yaml").Output()
	if err != nil {
		return nil, err
	}

	encrypted := newEncryptedTemplate()
	err = yaml.Unmarshal(out, encrypted)
	if err != nil {
		return nil, err
	}

	return encrypted, nil
}

# Seal Secrets

## Description

Seal secrets is a simple command line tool written in Go that uses Go templates and regex in order to easily add sealedSecrets to your helm chart.

Where you would normally have to manually create a plainTemplate containing the secrets to be sealed, including production secrets, you can now add them directly into their designated value files and refer to them in your sealedSecret template.
The tool will then automatically find your secret and encrypt it. 

Note: since the tool uses a lot of regex, it isn't going to be completely foolproof. So although it's been tested successfully on large and complex helm charts, there will very likely be structures that it won't understand.

## Installation

I haven't gotten around to creating downloadable binaries yet, so for now you'll have to [install go](https://go.dev/doc/install).

Make sure your PATH contains the go binary and the private go binary directory ~/go/bin !!!!!

Once Go is installed, you can either run 

`$ go install gitlab.com/EDSN/team-visual/seal-secrets`

or

`$ go install . ` inside the cloned repo

Run `$ seal-secrets` to see if it's been installed correctly. It should show a generic usage message. 


## Usage

When using seal-secrets on a helm chart for the first time, you'll want to start with init command. 
This will analyze the chart and find any existing sealed secrets based on the secret templates and corresponding values in value files. 
It will then create a _sealed.yaml file and add the keys to those values to it, as to understand which secrets have already been sealed.

Example:
```shell
# -v takes in comma-separated value files. Seal-secrets is sadly incapable of recognizing value files on its own.
seal-secrets init -v dev.yaml,acc.yaml,values.yaml ./path/to/chart
```

When adding a new secret, follow below steps:

1. Add your plain secret to a value file
2. Add the reference to that value to the SealedSecret template that should contain it like
    ```yaml
    spec:
      encryptedData:
        mySecret: {{ .Values.container.someSecret }}
    ```
3. Make sure that changed value files contain a 'secretNamespace' and 'secretName' key, referring to the namespace/name in which your secrets are going to be deployed and unsealed. You can override these by using the --name and --namespace flags.
4. Run the following command, using the value file(s) that you added your secret to 
    ```shell
    # The default certpath is http://sealed-secrets.local.rhos-ota.tnl-edsn.nl/v1/cert.pem
    seal-secrets seal -v dev.yaml,acc.yaml --certpath 'http://www.my-pub-key.nl/cert.pem' ./path/to/chart
    ``` 
5. When sealing non plain-text (base64 encoded) values, use the ```-base64``` flag
6Check that the secret has been properly sealed

